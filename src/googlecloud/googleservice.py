from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from googleapiclient.errors import HttpError

import os


class GoogleService:
    """
    This class is to build a service to access google service API. During the creation of service, Oauth 2.0 is required.

    """

    def __init__(self, client_secret_file, token_file, api_name, api_version, scopes):
        self.CLIENT_SECRET_FILE = client_secret_file
        self.TOKEN_FILE = token_file
        self.API_SERVICE_NAME = api_name
        self.API_VERSION = api_version
        self.SCOPES = scopes
        self.cred = None

    def get_service(self):
        cred = self.cred

        # if OAuth 2.0 is complete before, it will save a token file in json format.
        # Therefore, it can reload it for authentication or renewal.
        if not cred and os.path.exists(self.TOKEN_FILE):
            cred = Credentials.from_authorized_user_file(self.TOKEN_FILE, self.SCOPES)

        if not cred or not cred.valid:
            if cred and cred.expired and cred.refresh_token:
                cred.refresh(Request())
            else:
                # First time to do OAuth
                flow = InstalledAppFlow.from_client_secrets_file(
                    self.CLIENT_SECRET_FILE, self.SCOPES)
                # After the authentication succeeds in the browser via the URL provided,
                # Google will send a redirect URL which triggers a call to localhost server
                # in order to get the token back in this program.
                self.cred = flow.run_local_server(port=0)
                cred = self.cred

            # write the token to a file for future authentication automatically
            with open(f'{self.TOKEN_FILE}', 'w') as token:
                token.write(cred.to_json())

        try:
            return build(self.API_SERVICE_NAME, self.API_VERSION, credentials=cred)
        except HttpError as error:
            print(f'Failed to create google service: {error}')
            raise error
