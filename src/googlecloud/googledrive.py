from googleapiclient.errors import HttpError
from googleapiclient.http import MediaFileUpload
from googlecloud.googleservice import GoogleService

import os
import logging.config

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


class GoogleDrive:
    """
    This class is to create an object for accessing Google Drive service.

    """

    def __init__(self, client_secret_file, token_file, api_name, api_version, scopes):
        self.__google_service = GoogleService(client_secret_file, token_file, api_name, api_version, scopes)
        self.__google_service.get_service()  # init authentication to google

    def __get_service(self):
        return self.__google_service.get_service()

    # create a folder
    def create_folder(self, folder_name, parents=None):
        try:
            with self.__get_service() as service:
                file_metadata = {
                    "name": folder_name,
                    "mimeType": "application/vnd.google-apps.folder"
                }

                if parents:
                    file_metadata["parents"] = parents

                file = service.files().create(body=file_metadata, fields='id').execute()
                logger.info(f'Folder has created with ID: "{file.get("id")}".')
                return file.get('id')

        except HttpError as error:
            logger.error(f'Failed to create folder {folder_name}: {error}')
            raise error

    # search files with specified criteria
    def search_file(self, filename, include_trash=False, mime_type=None, parent_id=None):
        try:
            with self.__get_service() as service:
                files = []
                page_token = None
                while True:
                    q = None
                    if filename:
                        q = f"name = '{filename}'"
                    if mime_type:
                        if q:
                            q += " and "
                        q += f"mimeType = '{mime_type}'"
                    if parent_id:
                        if q:
                            q += " and "
                        q += f"'{parent_id}' in parents"

                    q += f" and trashed = {str(include_trash).lower()}"

                    response = service.files().list(q=q,
                                                    spaces='drive',
                                                    fields='nextPageToken, '
                                                           'files(id, name)',
                                                    pageToken=page_token).execute()
                    files.extend(response.get('files', []))
                    page_token = response.get('nextPageToken', None)
                    if page_token is None:
                        break

                return files
        except HttpError as error:
            logger.error(f'Failed to search {filename}: {error}')
            raise error

    # upload a file
    def upload_file(self, file_path, mime_type=None, parents=None):
        try:
            with self.__get_service() as service:
                filename = os.path.basename(file_path)
                file_metadata = {'name': f'{filename}'}

                if parents:
                    file_metadata["parents"] = parents

                media = MediaFileUpload(file_path, mimetype=mime_type)
                file = service.files().create(body=file_metadata, media_body=media,
                                              fields='id').execute()
                logger.info(f'File id: {file.get("id")}')
            return file.get('id')
        except HttpError as error:
            logger.error(f'Failed to upload a file {file_path}: {error}')
            raise error
