from googlecloud.googledrive import GoogleDrive
import pathlib

if __name__ == '__main__':
    current_path = pathlib.Path(__file__).parent.absolute().__str__() + "/"
    client_secret_file = f"{current_path}client_secret.json"
    token_file = f"{current_path}token.json"
    # print(current_path)
    api_name = 'drive'
    api_version = 'v3'
    scopes = ["https://www.googleapis.com/auth/drive"]
    google_drive = GoogleDrive(client_secret_file, token_file, api_name, api_version, scopes)
    file_id = google_drive.create_folder("hello")
    files = google_drive.search_file(filename="hello", mime_type="application/vnd.google-apps.folder")
    # print(files)
    google_drive.upload_file( f"{current_path}test.txt", parents=[file_id])
